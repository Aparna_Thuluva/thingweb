# thingweb #

Thingweb is an open source implementation of the WoT servient model of W3C's interest group on the web of things (W3C WoT IG).

### Building ###

* we are using [Gradle](https://gradle.org/) as build tool
* the application is built using the [Gradle Application Plugin](https://docs.gradle.org/current/userguide/application_plugin.html)

There is a also build setup on [drone.io](https://drone.io/bitbucket.org/ascholz/thingweb/latest). 
[![Build Status](https://drone.io/bitbucket.org/ascholz/thingweb/status.png)](https://drone.io/bitbucket.org/ascholz/thingweb/latest)


### Contribution guidelines ###

* we are happy for contributions
* just open a PR
* Writing tests will be mandatory soon
* Peer code review will take place

### TODO List (adopt a topic by PR-ing your name) ###

- [ ] code for LED example
- [ ] client UI
- [ ] interface for token checking
- [ ] client-side code
- [ ] API documentation
- [ ] serialization-agnostic data types
- [ ] further protocol bindings for XMPP and WS
- [ ] test framework
- [ ] tests

### Who do I talk to? ###

* W3C WoT IG Members from Siemens
