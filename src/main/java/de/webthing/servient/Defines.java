package de.webthing.servient;


public class Defines {

	public static final String BASE_URL = "/";
	
	public static final String BASE_THING_URL = BASE_URL + "things/";
	
	public static final String BASE_TD_URL = BASE_URL + "tds/";
	
	public static final String REL_PROPERTY_URL = "/properties/";
	
	public static final String REL_ACTION_URL = "/actions/";
}
