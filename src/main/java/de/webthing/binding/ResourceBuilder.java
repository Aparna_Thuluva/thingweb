package de.webthing.binding;

public interface ResourceBuilder {

	void newResource(String url, RESTListener restListener);
}
